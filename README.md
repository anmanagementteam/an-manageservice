* Standard callback response object for methods and services:
{success: Boolean, message: Object, responses: [{success: Boolean, message: Object, response: {Error}||{Result}}]}

* Standard logging format
console.log(utils.dateToLog()+' - '+_method +' :: '+'Message');

