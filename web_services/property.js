var Property = require('../models/property');

Property.actions = {
    
    add: function (property, callback) {
        try {
            var core = null;
            if (property instanceof Array){
                property.forEach(function(p){
                     var newProperty = Property(p);
                     newProperty.save(function (err, result) {});
                });
                core = {res: null, success: true, error: null, message: "Success - save call for each property"};
                callback(null, null, core);    
            }else{
                var newProperty = Property(property);
                newProperty.save(function (err, result) {
                core = (err || result.length===0) ? {
                    success: false
                    , error: err
                    , message: "Error - Could not add property"
                } : {
                    res: result
                    , success: true
                    , error: null
                    , message: "Success - property was added / " + result
                };

                callback(err, result, core);
                });
            }
        } catch (e) {console.log(e);}

    },

    getAll: function (callback) {
        try {
            Property.find({}, function (err, result) {
                var core = (err || result.length===0) ? {
                    success: false
                    , error: err
                    , message: "Error - Could not find properties"
                } : {
                    res: result
                    , success: true
                    , error: null
                    , message: "Success - Properties returned / " + result
                };

                callback(err, result, core);
            });
        } catch (e) {
            console.log(e);
        }
    }

}

module.exports = Property;