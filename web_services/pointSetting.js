var PointSetting = require('../models/point_setting');

PointSetting.actions = {
    
    add: function (pointSetting, callback) {
        try {
            var core = null;
            if (pointSetting instanceof Array){
                pointSetting.forEach(function(p){
                     var newPointSetting = PointSetting(p);
                     newPointSetting.save(function (err, result) {});
                });
                core = {res: null, success: true, error: null, message: "Success - save call for each pointSetting"};
                callback(null, null, core);    
            }else{
                var newPointSetting = PointSetting(pointSetting);
                newPointSetting.save(function (err, result) {
                core = (err || result.length===0) ? {
                    success: false
                    , error: err
                    , message: "Error - Could not add PointSetting"
                } : {
                    res: result
                    , success: true
                    , error: null
                    , message: "Success - PointSetting was added / " + result
                };
                callback(err, result, core);
                });
            }

        } catch (e) {console.log(e);}
    },

    getAll: function (callback) {
        try {
            PointSetting.find({}, function (err, result) {
                var core = (err || result.length===0) ? {
                    success: false
                    , error: err
                    , message: "Error - Could not find pointSettings"
                } : {
                    res: result
                    , success: true
                    , error: null
                    , message: "Success - pointSettings returned / " + result
                };

                callback(err, result, core);
            });
        } catch (e) {
            console.log(e);
        }
    }

}

module.exports = PointSetting;