var Tenant = require('../models/tenant');

Tenant.actions = {
    
    add: function (tenant, callback) {
        try {
            var core = null;
            if (tenant instanceof Array){
                tenant.forEach(function(t){
                     var newTenant = Tenant(t);
                     newTenant.save(function (err, result) {});
                });
                core = {res: null, success: true, error: null, message: "Success - save call for each tenant"};
                callback(null, null, core);    
            }else{
                var newTenant = Tenant(tenant);
                newTenant.save(function (err, result) {
                core = (err || result.length===0) ? {
                    success: false
                    , error: err
                    , message: "Error - Could not add Tenant"
                } : {
                    res: result
                    , success: true
                    , error: null
                    , message: "Success - Tenant was added / " + result
                };
                callback(err, result, core);
                });
            }
        } catch (e) {console.log(e);}

    },

    getAll: function (callback) {
        try {
            Tenant.find({}, function (err, result) {
                var core = (err || result.length===0) ? {
                    success: false
                    , error: err
                    , message: "Error - Could not find tenants"
                } : {
                    res: result
                    , success: true
                    , error: null
                    , message: "Success - Tenants returned / " + result
                };

                callback(err, result, core);
            });
        } catch (e) {
            console.log(e);
        }
    }

}

module.exports = Tenant;