var methods = require('../middle_layer/methods/calendar');

module.exports = function(app) {    

    // webservice APIs
    app.get('/calendars',function(request,response,next){
            methods.getCalendars(function(result){
                 response.json(result);
            });
    });

    app.get('/calendar/:year',function(request,response,next){
            methods.getCalendarByYear(request.params.year, function(result){
                 response.json(result);
            });
    });

    app.get('/calendar/:year/holidays',function(request,response,next){
            methods.getHolidaysByCalendarYear(request.params.year, function(result){
                 response.json(result);
            });
    });

    app.put('/calendars', function(request, response, next) {
            methods.addCalendars(request.body, function(result){
                 response.json(result);
            });
    });

    app.put('/calendars/add/holidays', function(request, response, next) {
            methods.addHolidaysToCalendar(request.body.holidays, request.body.calendars,
             function(result){
                 response.json(result);
            });
    });
    
    app.delete('/calendars', function(request, response, next) {
            methods.delete(request.body,function(result){
                 response.json(result);
            });
    });

    app.delete('/holiday/calendar/:year', function(request, response, next) {
            methods.delete(request.body,function(result){
                 response.json(result);
            });
    });



}