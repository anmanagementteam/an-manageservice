var PointTx = require('../models/pointTx');

PointTx.actions = {
    
    add: function (pointTx, callback) {
        try {
            var core = null;
            if (pointTx instanceof Array){
                pointTx.forEach(function(p){
                     var newPointTx = PointTx(p);
                     newPointTx.save(function (err, result) {});
                });
                core = {res: null, success: true, error: null, message: "Success - save call for each pointTx"};
                callback(null, null, core);    
            }else{
                var newPointTx = PointTx(pointTx);
                 newPointTx.save(function (err, result) {
                 core = (err || result.length===0) ? {
                    success: false
                    , error: err
                    , message: "Error - Could not add PointTx"
                } : {
                    res: result
                    , success: true
                    , error: null
                    , message: "Success - PointTx was added / " + result
                };
                callback(err, result, core);
                });
             }
        } catch (e) {console.log(e);}

    },
    

    getAll: function (callback) {
        try {
            PointTx.find({}, function (err, result) {
                var core = (err || result.length===0) ? {
                    success: false
                    , error: err
                    , message: "Error - Could not find pointsTx"
                } : {
                    res: result
                    , success: true
                    , error: null
                    , message: "Success - PointsTx returned / " + result
                };

                callback(err, result, core);
            });
        } catch (e) {
            console.log(e);
        }
    }

}

module.exports = PointTx;