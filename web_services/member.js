var Member = require('../models/member');

Member.actions = {

    add: function (member, callback) {
        try {
            var core = null;
            if (member instanceof Array){
                member.forEach(function(m){
                     var newMember = Member(m);
                     newMember.save(function (err, result) {});
                });
                core = {res: null, success: true, error: null, message: "Success - save call for each member"};
                callback(null, null, core);    
            }else{
                var newMember = Member(member);
                newMember.save(function (err, result) {
                    core = (err || result.length===0) ? {
                        success: false
                        , error: err
                        , message: "Error - Could not add member"
                    } : {
                        res: result
                        , success: true
                        , error: null
                        , message: "Success - User was added / " + result
                    };

                callback(err, result, core);
            });
            
            }
            
        } catch (e) {console.log(e);}

    },

    getAll: function (callback) {
        try {
            Member.find({}, function (err, result) {
                var core = (err || result.length===0) ? {
                    success: false
                    , error: err
                    , message: "Error - Could not find member"
                } : {
                    res: result
                    , success: true
                    , error: null
                    , message: "Success - Members returned / " + result
                };

                callback(err, result, core);
            });
        } catch (e) {
            console.log(e);
        }

    }

}

module.exports = Member;