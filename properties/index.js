var index = [];

var server = require('./server');
var config = require('./config');
//var ui = require('./ui');

index.server = server;
index.config = config;

var property = [];

property.get = function (route){
    var ret = null;
    try{
        route = route.toLowerCase();
        var split_route = route.split('.');
        ret = index;
        split_route.forEach(function(r){
                ret = ret[r];
        });
    }catch(e){};

    return ret;
};

property.server = server;
property.config= config;

module.exports = property;