var server = [];

// routes
server.url = [];
server.url.fn = [];

server.url.fn.holidays = function(year) {
    return 'http://www.webcal.fi/cal.php?id=52&format=json&start_year='+year+'&end_year='+year+'&tz=America%2FNew_York';
};

server.hostname = 'localhost';
server.port = '8080';

module.exports = server;