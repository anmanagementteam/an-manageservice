#!/bin/env node
var properties = require('./properties');
var bodyParser = require('body-parser');
var express = require('express');
var db = require('./database');
var http       = require('http');
var url        = require('url');
var fs         = require('fs');
var calendar = require('./web_services/calendar');

// web front

var app = module.exports = express();
var server = http.createServer(app);

/**
 *  Backend application definition.
 */
var Backend = function () {
        var this_backend = this;

        //var Core = require('./core');

         //var running = new Core();

        this.start = function () {

            var tables = [ {name:'calendars'},{name:'members'},{name:'pointSettings'},{name:'pointsTx'},{name:'properties'},{name:'tenants'}];
    

            console.log('Running');

            //calendar(app);
            app.configure(function(){
                app.use(bodyParser.json());
                app.use(app.router);
            });

            var port = process.env.PORT || properties.server.port;
            var host = process.env.HOST || properties.server.hostname;

            app.listen(port);

            calendar(app);

            app.post("/someRoute", function(req, res) {
                console.log(req.body);
                res.send({ status: 'SUCCESS' });
            });

            // server.listen(port, function(){
            //     console.log("Express server listening on host %s, port %d in %s mode", host, port, app.settings.env);
            // });

            server.on('error', function(e) {
                console.log('Error: '+e);
            });
            

        };
    }


/**
 *  main():  Main code.
 */

var backendApp = new Backend();
backendApp.start();