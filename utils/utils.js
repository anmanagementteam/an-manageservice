var Utils = [];

Utils.isEmpty = function (element){
	try{
		return Object.keys(element).length === 0;
	}catch(e){}	
	return true;
}

Utils.isFunction = function (element){
	try{
      return Object.prototype.toString.call(element) == '[object Function]';
	}catch(e){}
	return false;
}

Utils.dateToLog = function(){
	return new Date().toISOString();
}

Utils.dateToIsoStringWithOutTime = function(dateString){
	return new Date(dateString).toISOString().split('T')[0];
}

Utils.coutUnSuccess = function (responses){
    var fails = [];
    responses.forEach(function(response){
       if (response.success === false)
            fails.push(false);
    });
    return fails.length;
}

Utils.coutSuccess = function (responses){
    var fails = [];
    responses.forEach(function(response){
       if (response.success === true)
            fails.push(true);
       
    });
    return fails.length;
}

Utils.areAllUnSuccess = function (responses){
    return this.coutSuccess(responses)===0;
}

Utils.areAllSuccess = function (responses){
    return !this.coutUnSuccess(responses)>0;
}

module.exports = Utils;