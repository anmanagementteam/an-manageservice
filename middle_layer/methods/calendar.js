var Calendar = require('../models/calendar');
var methods = [];

    var coutUnSuccess = function (messages){
        var fails = [];
        messages.forEach(function(response){
            if (response.success == false)
                fails.push(false);
        });
        return fails.length;
    }

    // public Methods
    methods.getCalendarByYear = function(year, callback){
        Calendar.findOne({ year: year }, function(err, calendar) {
                (err || !calendar)?callback({success: false, error: err}):
                callback({success: true, result: calendar});
            });
    }

    methods.getHolidaysByCalendarYear = function(year, callback){
        Calendar.findOne({ year: year }, function(err, calendar) {
                (err || !calendar)?callback({success: false, error: err}):
                callback({success: true, result: calendar.holidays});
            });
    }

    methods.getCalendars = function(callback){
        Calendar.find({}).sort({ year: 1})
            .exec(function(err, calendars) {
                (err || !calendars)?callback({success: false, error: err}):
                callback({success: true, result: calendars});
            });
    }

    methods.addCalendars = function(calendars, callback){
        Calendar.add(calendars, function(err, result){
            if(err || !result)
                callback({success: false, error: err||result});
            else{
                var unSuccessResults = coutUnSuccess(result);
                if(result.length - unSuccessResults <= 0)
                    callback({success: false, error: err||result})
                else callback({success: true, result: result});
            }
        });
    }

    methods.addHolidaysToCalendar= function(holidays, calendars, callback){
        Calendar.addHolidays(holidays, calendars, function(response){
            callback(response);
        });
    }

    methods.delete= function(calendars, callback){
        Calendar.delete(calendars, function(err, result){
            if(err || !result)
                callback({success: false, error: err||result});
            else{
                var unSuccessResults = coutUnSuccess(result);
                if(result.length - unSuccessResults <= 0)
                    callback({success: false, error: err||result})
                else callback({success: true, result: result});
            }
        });
    }

    methods.deleteHolidays= function(holidays, calendarYear, callback){
        Calendar.delete(calendars, function(err, result){
            if(err || !result)
                callback({success: false, error: err||result});
            else{
                var unSuccessResults = coutUnSuccess(result);
                if(result.length - unSuccessResults <= 0)
                    callback({success: false, error: err||result})
                else callback({success: true, result: result});
            }
        });
    }

module.exports = methods;