var mongoose = require('mongoose');

var ObjectId = mongoose.Schema.Types.ObjectId;

var PointSettingSchema = mongoose.Schema({
    point_value: {
        type: Number,
        default: 1.25
    },
    min_points_rendeem: {
        type: Number,
        default: 50
    },
    points_rendeem_charge: {
        type: Number,
        default: 5
    },
    if_rendeem_and_late_months_inactive:{
        type: Number,
        default: 3
    },
    if_start:{
        late_pays_allowed: {type: Number, default: 1},
        period_for_late_pays_allowed: {type: Number, default: 12}, //in Number of months
        additional_bonus_for_on_time_pay: {type: Number, default: 1},
    },
    lose_start:{
        late_pay_number: {type: Number, default: 2},
        period: {type: Number, default: 12} //in Number of months
    },
    bonuses: [{ //calculation to be made based on payment's day
        base_date: { type: Date , default: Date.now }, // date to be take in consideration, if 01/04/2016 it would take as reference each 01er
        day: {type: Number, min: 1, max: 31}, // this will be by default the date value day, could be changed
        days: {type: Number}, //if number of days needs between payments is going to be used to add bonuses
        points: {type: Number, default: 1},
        frenquency: {type: Number, default: 1} // monthly count, ex 1-> each month..
    }],
    penalties: [{
        base_date: {type: Date , default: Date.now}, // date to be take in consideration, if 01/04/2016 it would count days from day 1
        days: {type: Number, default: 31}, //Number of days without paying
        points: {type: Number, default: 0},
        porcentage: {type: Number},
        frenquency: {type: Number, default: 1} // monthly count, ex 1-> each month..
    }],
    due_date:{ // when due date setted means has a valid time period and when completed user should return to use default settings
        type: Date,
        default: null
    }
});

var PointSetting = mongoose.model('PointSetting',PointSettingSchema);
module.exports = PointSetting;