var mongoose = require('mongoose');

var MemberSchema = mongoose.Schema({
    name: {
        first: {type: String},
        last:  {type: String}
    },
    phone: {
        type: String
    },
    email: {
        type: String,
        unique: true
    }
});

var Member = mongoose.model('Member',MemberSchema);
module.exports = Member;