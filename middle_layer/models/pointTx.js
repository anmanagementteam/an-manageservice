var mongoose = require('mongoose');

var ObjectId = mongoose.Schema.Types.ObjectId;

var PointTxSchema = mongoose.Schema({
    tenant_id: {
        type: ObjectId,
        ref: 'tenant',
        required: true
    },
    payment_id: {
        type: ObjectId,
        ref: 'payment'
    },
    value: {
        points: {type: Number, default: 1},
        valued_at: {type: Number, default: 1.25}
    },
    type: {
        type: String,
        enum: ['rent', 'bonus', 'penalty', 'redemption'],
        default: 'rent'
    },
    description: { 
        type: String,
        default: 'point for rent payment'
    },
    date: {
      type: Date,
      default: Date.now
    },
    is_valid: {
        type: Boolean, 
        default: true
    }
});

var PointTx = mongoose.model('PointTx',PointTxSchema);
module.exports = PointTx;