var mongoose = require('mongoose');

var PropertySchema = mongoose.Schema({
    name: {
        type: String,
        unique: true,
        required: true
    },
    location: {
        type: String
    },
    rent_cost: {
        type: String
    }
});

var Property = mongoose.model('Property',PropertySchema);
module.exports = Property;