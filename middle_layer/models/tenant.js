var mongoose = require('mongoose');

var ObjectId = mongoose.Schema.Types.ObjectId;

var Payment = mongoose.Schema({
    date: {type: Date, default: Date.now},
    month: {type: Number}, //by default will be the currently month, if user has already paid that currently month will be the next month
    type: {type: String, default: 'onTime', enum: ['late','onTime','noLate'] }
});

var TenandSchema = mongoose.Schema({
    member_id: {
        type: ObjectId,
        required: true
    },
    property_id: {
        type: ObjectId,
        required: true
    },
    point_setting: {
        type: ObjectId,
        default: "507f1f77bcf86cd799439011"
    },
    date: { 
        type: Date, 
        default: Date.now
    },
    is_start: {
        type: Boolean, 
        default: false
    },
    is_bonus_inactive: {
        type: Boolean, 
        default: false  
    },
    payments: [Payment]
});

var Tenand = mongoose.model('Tenand',TenandSchema);
module.exports = Tenand;