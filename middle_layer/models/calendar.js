var mongoose = require('mongoose');
var async    = require('async');
var utils    = require('../../utils/utils');

var Holiday = mongoose.Schema({
    date: {
        type: Date,
        required: true,
        unique: true
    },
    name: {
        type: String,
        required: true,
        unique: true
    }
});

//var Holiday = mongoose.model('Holiday',Holiday);

var Calendar = mongoose.Schema({
    date: {
        type: Date,
        default: Date.now
    },
    year: {
        type: String,
        required: true,
        unique: true
    },
    holidays: [Holiday]
});
Calendar.index({ year: 1 });
Calendar.plugin(require('mongoose-lifecycle'));

// methods within the Model's objects
Calendar.methods.isHolidayInCalendar =  function(holiday){
    _holiday = holiday;
    var isIn = this.holidays.some(function (holiday) {
                    return ( (holiday.name.toLowerCase() === _holiday.name.toLowerCase()) || (holiday.date === _holiday.date) ||( (new Date(holiday.date).getFullYear() === new Date(_holiday.date).getFullYear()) &&
                     (new Date(holiday.date).getDay() === new Date(_holiday.date).getDay()) && (new Date(holiday.date).getMonth() === new Date(_holiday.date).getMonth()) ) );
                });
    return isIn;
}

Calendar.methods.isHolidayWithRequiredFields=  function(holiday){
    return ( (holiday.name) && (holiday.name.toString()!== '') && (holiday.date) && (holiday.date.toString()!== '') );
}

Calendar.methods.verifyHoliday = function(holiday){
    if (!this.isHolidayWithRequiredFields(holiday))
        return false;
    if( new Date(holiday.date).getFullYear().toString() !== this.year.toString() )
        return false;
    if (this.isHolidayInCalendar(holiday))
        return false;
    return true;
}

// statics methods and Objects within the Model
Calendar.statics.Holiday = mongoose.model('Holiday',Holiday);

Calendar.statics.addHolidays = function(holidays, calendars, callback) {
    var _method = 'Calendar.static.addHolidays';
    var responses = []; 

    if (!utils.isFunction(callback)){
        console.log(utils.dateToLog()+' - '+_method +' :: '+'Callback is not a function');
        return;
    }
    if (utils.isEmpty(holidays)){
        console.log(utils.dateToLog()+' - '+_method +' :: '+'No holidays to add');
        callback({success: false, message:'No holidays to add', responses: null});
        return;
    }
    if (utils.isEmpty(calendars)){
        console.log(utils.dateToLog()+' - '+_method +' :: '+'No calendars to add');
        callback({success: false, message:'No calendars to add', responses: null});
        return;
    }

    var _this = this;
    calendars = (calendars instanceof Array)? calendars : [calendars];
    holidays = (holidays instanceof Array)? holidays : [holidays];

    async.each(calendars,function(calendar, calendarFinished){
        var _calendarAsync = calendar;
        _this.findOne(calendar, function (err, calendar){
            if(utils.isEmpty(calendar) || err){
                responses.push({success: false, message: 'This calendar was not found', response: calendar||err});
                calendarFinished();
            }else {
                var holidaysResponses = [];
                async.each(holidays,function(holiday, holidayFinished){
                    holiday.date = utils.dateToIsoStringWithOutTime(holiday.date); // to avoid using time when day/month entered without 0 ex. 2016/1/1 should be 2016/01/01  
                    holiday = _this.Holiday(holiday);
                    if(calendar.verifyHoliday(holiday)){
                        calendar.update({$push: {holidays: holiday}}, { w: 1}, function(err,response){
                            var success = (err)?(false):utils.isEmpty(response)?(false):(true);
                            holidaysResponses.push({success:success, message:{input:holiday,interface:success?'This Holiday was added':'Holiday was no added',error:err}, response:response});
                            holidayFinished();
                         });
                    }else{
                        holidaysResponses.push({success:false, message:{input:holiday,interface:"Holiday is already in or doesn't match criteria"}, response:holiday});
                        holidayFinished();
                    }
                }, function(err){
                    var success = (err)?(false):utils.isEmpty(holidaysResponses)?(false):utils.areAllUnSuccess(holidaysResponses)?false:true;
                    responses.push({success:success, message:{input:_calendarAsync,interface: success?'The call was successfully completed for this calendar':'All the records failed for this calendar', error:err}, responses:holidaysResponses});
                    calendarFinished();
                });
            }
        });
    }, function(err){
        var success = (err)?(false):utils.isEmpty(responses)?(false):utils.areAllUnSuccess(responses)?false:true;
        callback({success: success, message:{interface: success?'The call was successfully completed':'The call was successfully completed without changes', error:err}, responses:responses});
    });

}

Calendar.statics.add = function(calendars, callback) {
    var _this = this;
    var savedResponses = [];
    calendars = (calendars instanceof Array)? calendars : [calendars];
        if (calendars.lenght == 0) callback({message: 'no calendar found'} , null);
        async.each(calendars,function(calendar, callback){
            calendar = _this(calendar);
            calendar.save(function(err,result){
                savedResponses.push({success: ((err)?false:result?true:false) , calendar: calendar, result: result||err});
                callback();
            });
        }, function(err){
                callback(null,savedResponses);
            }
        );
};

Calendar.statics.delete = function(calendars, callback) {
    var _this = this;
    var deletedResponses = [];
    calendars = (calendars instanceof Array)? calendars : [calendars];
        if (calendars.lenght == 0) callback({message: 'no calendar found'} , null);

        async.each(calendars,function(calendar, callback){
            _this.findOneAndRemove(calendar,function(err, result){
                deletedResponses.push({success: ((err)?false:result?true:false) , calendar: calendar, result: result||err});
                callback();
            });
        }, function(err){
                callback(null,deletedResponses);
            }
        );
};

// methods within the Holiday Model Objects

// // methods within the Holiday Model


module.exports = mongoose.model('Calendar',Calendar);
